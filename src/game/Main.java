/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

/**
 *
 * @author Admin
 */
import java.util.ArrayList;

import java.util.Random;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Scanner;

public class Main {

   public static int getPoints(ArrayList<Integer> hand) {
       Card nCard = new Card();
       int[] cards = new int[14];
       int points = 0;
// Count the number of cards of each value.
       for (int card : hand) {
           cards[card]++;
       }
// Remove any pairs of cards. We remove pairs as a separate operation
// from counting the number of cards above to prevent locking and other
// issues
       for (int value = 1; value < 14; value++) {
           if (cards[value] > 1) {
               System.out.println(" Found " + (cards[value] / 2) * 2 + " " + value + "'s");
               int cardsRemoved = nCard.removeCards(hand, value, (cards[value] / 2) * 2);
               points += cardsRemoved / 2;
           }
       }
       return points;

   }

   public static int handleTurn(ArrayList<Integer> deck, ArrayList<Integer> thisHand, ArrayList<Integer> otherHand,
           boolean humansTurn) {
      Card newCard = new Card();
       Random rand = new Random();
       if (thisHand.size() == 0) {
           thisHand.add(newCard.getCard(deck));
       }
       int requestedCard = 0;

       if (humansTurn) {
// If it's the human's turn, ask them for a card value1

           Scanner in = new Scanner(System.in);
           do {
               System.out.print(" Pick a card to request: ");
               requestedCard = in.nextInt();
               if (requestedCard < 0 || requestedCard > 13) {
                   System.out.println(" That's an invalid card...try again");
               }
           } while (requestedCard < 0 || requestedCard > 13);
       } else {
// if it's the computer's turn, randomly pick a card from their hand
           requestedCard = rand.nextInt(13) + 1;
           System.out.println(" Computer picked: " + requestedCard);
       }
       int cardsRemoved = newCard.removeCards(otherHand, requestedCard, 4);

       if (cardsRemoved == 0) {
// if no cards of the requested value were found in the opponent's hand,
// draw a card from the deck if there are cards in the deck
           System.out.println(" No cards found in opponent's hand. Go Fish!");
           int cardDrawn = newCard.getCard(deck);

           if (cardDrawn > 0) {
               thisHand.add(cardDrawn);
               System.out.println(" Added a " + cardDrawn + " to player's hand.");
           } else {
               System.out.println(" No cards left in deck.");
           }
       } else {
// if cards of the requested value were found in the opponent's hand,
// move them to the player's hand
           System.out.println(" Number of cards found in opponent's hand: " + cardsRemoved);
           for (int i = 0; i < cardsRemoved; i++) {
               thisHand.add(requestedCard);
           }
       }
       return getPoints(thisHand);
   }

   public static void main(String[] args) {

       // Generates initial deck of cards
       Deck myDeck = new Deck();
       Card myCard = new Card();

       ArrayList<Integer> deck = myDeck.getDeck();
       ArrayList<Integer> humanHand = new ArrayList<Integer>();
       ArrayList<Integer> computerHand = new ArrayList<Integer>();

       for (int i = 0; i < 7; i++) {
           humanHand.add(myCard.getCard(deck));
           computerHand.add(myCard.getCard(deck));
       }

       int humanPoints = 0;
       int computerPoints = 0;
// Process each turn starting with the human until no cards
// remain in either player's hands
       boolean humansTurn = true;
       while (humanHand.size() > 0 || computerHand.size() > 0) {
           System.out.println("Human: " + humanPoints + " points. " + "Computer: " + computerPoints + " points. "
                   + "Deck size: " + deck.size());
           if (humansTurn) {
               System.out.println("Human's turn");
               humanPoints += handleTurn(deck, humanHand, computerHand, true);
               System.out.println("HUman Hand:"+humanHand);
               humansTurn = false;
           } else {
               System.out.println("Computer's turn");
               computerPoints += handleTurn(deck, computerHand, humanHand, false);
               System.out.println("Computrt Hand:"+computerHand);
               humansTurn = true;
           }
           System.out.println();
       }

// Determine the winner

       if (humanPoints > computerPoints) {
           System.out.println("Human wins!");
       } else if (computerPoints > humanPoints) {
           System.out.println("Computer wins!");
       } else {
           System.out.println("Tie");
       }

   }
}
