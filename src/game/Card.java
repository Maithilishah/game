/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;
import java.util.ArrayList;
import java.util.Random;
/**
 *
 * @author Admin
 */

public class Card {

   // Random card is selected using random generator and the random card is removed
   // for the deck

   public static int getCard(ArrayList<Integer> deck) {
       Random rand = new Random();
      
       if (deck.size() == 0) {
           return 0;
       }
       int i = (int) (Math.random() * deck.size());
       int card = deck.get(i);
       deck.remove(i);
       return card;
   }

   public int removeCards(ArrayList<Integer> hand, int valueToRemove, int maxCardsToRemove) {
       int cardsRemoved = 0;

       // Look through the entire hand for the requested value
       for (int i = 0; i < hand.size();) {
// if we found the requested value, remove it from the hand
           if (hand.get(i) == valueToRemove) {

               hand.remove(i);
               cardsRemoved++;
               // if we've reached our limit on cards to remove, end the loop
               if (cardsRemoved >= maxCardsToRemove) {
                   break;
               }
           } else {
               // Since we're removing cards as we go, only increment if
               // the card didn't match
               i++;
           }
       }
       return cardsRemoved;
   }

}


